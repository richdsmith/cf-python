.. currentmodule:: cf
.. default-role:: obj

cf.Query
========

.. autoclass:: cf.Query
   :no-members:
   :no-inherited-members:

Attributes
----------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst
   
   ~cf.Query.attr
   ~cf.Query.exact
   ~cf.Query.operator
   ~cf.Query.value
   
Methods
-------

.. autosummary::
   :toctree: ../generated/
   :template: method.rst
   
   ~cf.Query.__init__
   ~cf.Query.addattr
   ~cf.Query.copy
   ~cf.Query.dump
   ~cf.Query.equals
   ~cf.Query.equivalent
   ~cf.Query.evaluate
   ~cf.Query.inspect
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
   
