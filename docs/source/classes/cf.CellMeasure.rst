.. currentmodule:: cf
.. default-role:: obj

cf.CellMeasure
==============

.. autoclass:: cf.CellMeasure
   :no-members:
   :no-inherited-members:

CF properties
-------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.CellMeasure.add_offset
   ~cf.CellMeasure.calendar
   ~cf.CellMeasure.comment
   ~cf.CellMeasure._FillValue
   ~cf.CellMeasure.history
   ~cf.CellMeasure.leap_month
   ~cf.CellMeasure.leap_year
   ~cf.CellMeasure.long_name
   ~cf.CellMeasure.missing_value
   ~cf.CellMeasure.month_lengths
   ~cf.CellMeasure.scale_factor
   ~cf.CellMeasure.standard_name
   ~cf.CellMeasure.units
   ~cf.CellMeasure.valid_max
   ~cf.CellMeasure.valid_min
   ~cf.CellMeasure.valid_range

Attributes
----------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.CellMeasure.array
   ~cf.CellMeasure.binary_mask
   ~cf.CellMeasure.data
   ~cf.CellMeasure.dtarray
   ~cf.CellMeasure.dtype
   ~cf.CellMeasure.hardmask
   ~cf.CellMeasure.isscalar
   ~cf.CellMeasure.mask
   ~cf.CellMeasure.ndim
   ~cf.CellMeasure.shape
   ~cf.CellMeasure.size
   ~cf.CellMeasure.subspace
   ~cf.CellMeasure.Units
   ~cf.CellMeasure.varray

Methods
-------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.CellMeasure.all
   ~cf.CellMeasure.allclose
   ~cf.CellMeasure.any
   ~cf.CellMeasure.asdatetime
   ~cf.CellMeasure.asreftime
   ~cf.CellMeasure.attributes
   ~cf.CellMeasure.chunk
   ~cf.CellMeasure.ceil
   ~cf.CellMeasure.clip
   ~cf.CellMeasure.close
   ~cf.CellMeasure.copy
   ~cf.CellMeasure.cos
   ~cf.CellMeasure.datum
   ~cf.CellMeasure.delprop
   ~cf.CellMeasure.dump
   ~cf.CellMeasure.equals
   ~cf.CellMeasure.exp
   ~cf.CellMeasure.expand_dims
   ~cf.CellMeasure.files
   ~cf.CellMeasure.fill_value
   ~cf.CellMeasure.flip
   ~cf.CellMeasure.floor
   ~cf.CellMeasure.getprop
   ~cf.CellMeasure.hasprop
   ~cf.CellMeasure.HDF_chunks
   ~cf.CellMeasure.identity
   ~cf.CellMeasure.insert_data
   ~cf.CellMeasure.inspect
   ~cf.CellMeasure.log
   ~cf.CellMeasure.mask_invalid
   ~cf.CellMeasure.match
   ~cf.CellMeasure.max
   ~cf.CellMeasure.mean
   ~cf.CellMeasure.min
   ~cf.CellMeasure.mid_range
   ~cf.CellMeasure.name
   ~cf.CellMeasure.override_calendar
   ~cf.CellMeasure.override_units
   ~cf.CellMeasure.properties
   ~cf.CellMeasure.range
   ~cf.CellMeasure.rint
   ~cf.CellMeasure.roll
   ~cf.CellMeasure.sample_size
   ~cf.CellMeasure.sd
   ~cf.CellMeasure.select
   ~cf.CellMeasure.setprop
   ~cf.CellMeasure.sin
   ~cf.CellMeasure.squeeze
   ~cf.CellMeasure.sum
   ~cf.CellMeasure.transpose
   ~cf.CellMeasure.trunc
   ~cf.CellMeasure.unique
   ~cf.CellMeasure.var
   ~cf.CellMeasure.where
