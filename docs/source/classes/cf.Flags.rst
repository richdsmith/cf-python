.. currentmodule:: cf
.. default-role:: obj

cf.Flags
========

.. autoclass:: cf.Flags
   :no-members:
   :no-inherited-members:

Attributes
----------
   
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Flags.flag_masks
   ~cf.Flags.flag_meanings
   ~cf.Flags.flag_values

Methods
-------
   
.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Flags.copy
   ~cf.Flags.dump
   ~cf.Flags.equals
   ~cf.Flags.sort
