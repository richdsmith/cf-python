.. currentmodule:: cf
.. default-role:: obj


cf.Coordinate
=============

.. autoclass:: cf.Coordinate
   :no-members:
   :no-inherited-members:

CF properties
-------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Coordinate.add_offset
   ~cf.Coordinate.axis
   ~cf.Coordinate.calendar
   ~cf.Coordinate.comment
   ~cf.Coordinate._FillValue
   ~cf.Coordinate.history
   ~cf.Coordinate.leap_month
   ~cf.Coordinate.leap_year
   ~cf.Coordinate.long_name
   ~cf.Coordinate.missing_value
   ~cf.Coordinate.month_lengths
   ~cf.Coordinate.positive
   ~cf.Coordinate.scale_factor
   ~cf.Coordinate.standard_name
   ~cf.Coordinate.units
   ~cf.Coordinate.valid_max
   ~cf.Coordinate.valid_min
   ~cf.Coordinate.valid_range
 
Attributes
----------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.Coordinate.array
   ~cf.Coordinate.binary_mask
   ~cf.Coordinate.bounds
   ~cf.Coordinate.cellsize
   ~cf.Coordinate.ctype
   ~cf.Coordinate.data
   ~cf.Coordinate.dtarray
   ~cf.Coordinate.dtype
   ~cf.Coordinate.day
   ~cf.Coordinate.hardmask
   ~cf.Coordinate.hasbounds
   ~cf.Coordinate.hour
   ~cf.Coordinate.isauxiliary
   ~cf.Coordinate.isdimension
   ~cf.Coordinate.isscalar
   ~cf.Coordinate.lower_bounds
   ~cf.Coordinate.mask
   ~cf.Coordinate.minute
   ~cf.Coordinate.month
   ~cf.Coordinate.ndim
   ~cf.Coordinate.second
   ~cf.Coordinate.shape
   ~cf.Coordinate.size
   ~cf.Coordinate.subspace
   ~cf.Coordinate.T
   ~cf.Coordinate.unique
   ~cf.Coordinate.Units
   ~cf.Coordinate.upper_bounds
   ~cf.Coordinate.varray
   ~cf.Coordinate.X
   ~cf.Coordinate.Y
   ~cf.Coordinate.year
   ~cf.Coordinate.Z

Methods
-------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Coordinate.asauxiliary
   ~cf.Coordinate.asdimension
   ~cf.Coordinate.attributes
   ~cf.Coordinate.chunk
   ~cf.Coordinate.clip
   ~cf.Coordinate.close
   ~cf.Coordinate.contiguous
   ~cf.Coordinate.convert_reference_time
   ~cf.Coordinate.copy
   ~cf.Coordinate.cos
   ~cf.Coordinate.datum
   ~cf.Coordinate.delprop
   ~cf.Coordinate.dump
   ~cf.Coordinate.equals
   ~cf.Coordinate.expand_dims
   ~cf.Coordinate.files
   ~cf.Coordinate.fill_value
   ~cf.Coordinate.flip
   ~cf.Coordinate.getprop
   ~cf.Coordinate.hasprop
   ~cf.Coordinate.HDF_chunks
   ~cf.Coordinate.identity
   ~cf.Coordinate.insert_bounds
   ~cf.Coordinate.insert_data
   ~cf.Coordinate.mask_invalid
   ~cf.Coordinate.match
   ~cf.Coordinate.name
   ~cf.Coordinate.override_units
   ~cf.Coordinate.properties
   ~cf.Coordinate.select
   ~cf.Coordinate.setprop
   ~cf.Coordinate.sin
   ~cf.Coordinate.squeeze
   ~cf.Coordinate.transpose
   ~cf.Coordinate.where
