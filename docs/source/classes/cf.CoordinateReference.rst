.. currentmodule:: cf
.. default-role:: obj

cf.CoordinateReference
======================

.. autoclass:: cf.CoordinateReference
   :no-members:
   :no-inherited-members:

Attributes
----------
 
.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.CoordinateReference.T
   ~cf.CoordinateReference.X
   ~cf.CoordinateReference.Y
   ~cf.CoordinateReference.Z
   ~cf.CoordinateReference.hasbounds
   
Methods
-------
 
.. autosummary::
   :toctree: ../generated/
   :template: method.rst
   
   ~cf.CoordinateReference.canonical_units
   ~cf.CoordinateReference.change_coord_identities
   ~cf.CoordinateReference.clear
   ~cf.CoordinateReference.close
   ~cf.CoordinateReference.copy
   ~cf.CoordinateReference.default_value
   ~cf.CoordinateReference.dump
   ~cf.CoordinateReference.equals
   ~cf.CoordinateReference.equivalent
   ~cf.CoordinateReference.get
   ~cf.CoordinateReference.has_key
   ~cf.CoordinateReference.identity
   ~cf.CoordinateReference.inspect
   ~cf.CoordinateReference.items
   ~cf.CoordinateReference.iteritems
   ~cf.CoordinateReference.iterkeys
   ~cf.CoordinateReference.itervalues
   ~cf.CoordinateReference.keys
   ~cf.CoordinateReference.match
   ~cf.CoordinateReference.pop
   ~cf.CoordinateReference.popitem
   ~cf.CoordinateReference.remove_all_coords
   ~cf.CoordinateReference.set
   ~cf.CoordinateReference.setcoord
   ~cf.CoordinateReference.setdefault
   ~cf.CoordinateReference.structural_signature
   ~cf.CoordinateReference.update
   ~cf.CoordinateReference.values
