.. cf-python documentation master file, created by
   sphinx-quickstart on Wed Aug  3 16:28:25 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. currentmodule:: cf
.. default-role:: obj

cf-python |release| documentation
=================================

.. note:: For version 1.x documentation, see the `documentation
          archive <https://cfpython.bitbucket.io/docs/archive.html>`_.

.. warning:: :ref:`Incompatible differences between versions 1.x and 2.x
             <one_to_two_changes>`

----

.. toctree::
   :maxdepth: 3
   
   introduction

----

.. toctree::
   :maxdepth: 3

   getting_started

----

.. toctree::
   :maxdepth: 3

   reference 

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

